<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

add_action( 'customize_register', 'landingpress_customize_settings' );
function landingpress_customize_settings( $wp_customize ) {

	if ( get_option( LANDINGPRESS_THEME_SLUG . '_license_key_status', false) != 'valid' ) {
		return;
	}
	
	// title_tagline = 20
	// colors = 40
	// header_image = 60
	// background_image = 80
	// static_front_page = 120

	$wp_customize->get_section( 'background_image' )->priority = 25;
	$wp_customize->get_section( 'background_image' )->title = esc_html__( 'Background', 'landingpress-wp' );
	$wp_customize->get_control( 'background_color' )->section = 'background_image';

	$wp_customize->add_panel( 'landingpress', array(
		'title'    => esc_html__( 'LandingPress', 'landingpress-wp' ),
		'priority' => 5
	) );

	$wp_customize->add_section( 'landingpress_pagelayout', array(
		'title'    => esc_html__( 'Page Layout', 'landingpress-wp' ),
		'panel'    => 'landingpress',
		'priority' => 2
	) );

	$wp_customize->add_section( 'landingpress_pagebuilder', array(
		'title'    => esc_html__( 'Page Builder', 'landingpress-wp' ),
		'panel'    => 'landingpress',
		'priority' => 2
	) );

	$wp_customize->add_section( 'landingpress_facebook_pixel', array(
		'title'    => esc_html__( 'Facebook Pixel', 'landingpress-wp' ),
		'panel'    => 'landingpress',
		'priority' => 3
	) );

	$wp_customize->add_section( 'landingpress_scripts', array(
		'title'    => esc_html__( 'Integrations (GTM, Analytics, Adwords, etc)', 'landingpress-wp' ),
		'panel'    => 'landingpress',
		'priority' => 3
	) );

	// $wp_customize->add_section( 'landingpress_google_tag_manager', array(
	// 	'title'    => esc_html__( 'Google Tag Manager', 'landingpress-wp' ),
	// 	'panel'    => 'landingpress',
	// 	'priority' => 3
	// ) );

	// $wp_customize->add_section( 'landingpress_google_analytics', array(
	// 	'title'    => esc_html__( 'Google Analytics', 'landingpress-wp' ),
	// 	'panel'    => 'landingpress',
	// 	'priority' => 3
	// ) );

	$wp_customize->add_section( 'landingpress_fonts', array(
		'title'    => esc_html__( 'Basic Fonts', 'landingpress-wp' ),
		'panel'    => 'landingpress',
		'priority' => 5
	) );

	$wp_customize->get_section( 'colors' )->title = esc_html__( 'Basic Colors', 'landingpress-wp' );
	$wp_customize->get_section( 'colors' )->panel = 'landingpress';
	$wp_customize->get_section( 'colors' )->priority = 10;

	$wp_customize->add_section( 'landingpress_customcss', array(
		'title'    => esc_html__( 'Custom CSS', 'landingpress-wp' ),
		'panel'    => 'landingpress',
		'priority' => 15
	) );

	$wp_customize->add_section( 'landingpress_header_script', array(
		'title'    => esc_html__( 'Header Script', 'landingpress-wp' ),
		'panel'    => 'landingpress',
		'priority' => 20
	) );

	$wp_customize->get_section( 'header_image' )->panel = 'landingpress';
	$wp_customize->get_section( 'header_image' )->priority = 25;
	$wp_customize->remove_control( 'header_textcolor' );
	$wp_customize->remove_control( 'display_header_text' );
	$wp_customize->get_control( 'header_image' )->section = 'landingpress_header';
	$wp_customize->get_control( 'header_image' )->priority = 5;

	$wp_customize->add_section( 'landingpress_header', array(
		'title'    => esc_html__( 'Header', 'landingpress-wp' ),
		'panel'    => 'landingpress',
		'priority' => 30
	) );

	$wp_customize->add_section( 'landingpress_header_menu', array(
		'title'    => esc_html__( 'Header Menu', 'landingpress-wp' ),
		'panel'    => 'landingpress',
		'priority' => 35
	) );

	$wp_customize->add_section( 'landingpress_breadcrumb', array(
		'title'    => esc_html__( 'Breadcrumb', 'landingpress-wp' ),
		'panel'    => 'landingpress',
		'priority' => 40
	) );


	$wp_customize->add_section( 'landingpress_template_home', array(
		'title'    => esc_html__( 'Home / Front Page', 'landingpress-wp' ),
		'panel'    => 'landingpress',
		'priority' => 100
	) );

	$wp_customize->add_section( 'landingpress_template_archive', array(
		'title'    => esc_html__( 'Blog / Archive Page', 'landingpress-wp' ),
		'panel'    => 'landingpress',
		'priority' => 105
	) );

	$wp_customize->add_section( 'landingpress_template_post', array(
		'title'    => esc_html__( 'Post Type - Post', 'landingpress-wp' ),
		'panel'    => 'landingpress',
		'priority' => 110
	) );

	$wp_customize->add_section( 'landingpress_template_page', array(
		'title'    => esc_html__( 'Post Type - Page', 'landingpress-wp' ),
		'panel'    => 'landingpress',
		'priority' => 115
	) );

	$wp_customize->add_section( 'landingpress_template_attachment', array(
		'title'    => esc_html__( 'Post Type - Attachment', 'landingpress-wp' ),
		'panel'    => 'landingpress',
		'priority' => 120
	) );

	$wp_customize->add_section( 'landingpress_template_comments', array(
		'title'    => esc_html__( 'Comments', 'landingpress-wp' ),
		'panel'    => 'landingpress',
		'priority' => 125
	) );

	$wp_customize->add_section( 'landingpress_template_sidebar', array(
		'title'    => esc_html__( 'Sidebar', 'landingpress-wp' ),
		'panel'    => 'landingpress',
		'priority' => 130
	) );

	$wp_customize->add_section( 'landingpress_footer_widgets', array(
		'title'    => esc_html__( 'Footer Widgets', 'landingpress-wp' ),
		'panel'    => 'landingpress',
		'priority' => 200
	) );

	$wp_customize->add_section( 'landingpress_footer', array(
		'title'    => esc_html__( 'Footer', 'landingpress-wp' ),
		'panel'    => 'landingpress',
		'priority' => 205
	) );

	$wp_customize->add_section( 'landingpress_footer_script', array(
		'title'    => esc_html__( 'Footer Script', 'landingpress-wp' ),
		'panel'    => 'landingpress',
		'priority' => 210
	) );

	$wp_customize->add_section( 'landingpress_optimization', array(
		'title'    => esc_html__( 'Optimization', 'landingpress-wp' ),
		'panel'    => 'landingpress',
		'priority' => 300,
		'description' => esc_html__( 'This is experimental feature. You can find some hidden gems here.', 'landingpress-wp' )
	) );

}

add_action( 'customize_register', 'landingpress_customize_settings_exportimport', 99 );
function landingpress_customize_settings_exportimport( $wp_customize ) {
	if ( function_exists('landingpress_exportimport_register') ) {
		$wp_customize->get_section( 'landingpress_exportimport_section' )->panel = 'landingpress';
	}
}

add_filter( 'landingpress_customize_controls', 'landingpress_customize_controls' );
function landingpress_customize_controls( $controls ) {

	if ( get_option( LANDINGPRESS_THEME_SLUG . '_license_key_status', false) != 'valid' ) {
		return $controls;
	}
	
	$controls[] = array(
		'type'     => 'radio-buttonset',
		'setting'  => 'landingpress_page_layout',
		'label'    => esc_html__( 'Page Layout', 'landingpress-wp' ),
		'section'  => 'landingpress_pagelayout',
		'default'  => 'boxed',
		'choices'  => array(
			'boxed' => esc_html__( 'Boxed', 'landingpress-wp' ),
			'fullwidth' => esc_html__( 'Full Width', 'landingpress-wp' ),
		),
	);

	$controls[] = array(
		'type'     => 'slider',
		'setting'  => 'landingpress_page_width',
		'label'    => esc_html__( 'Page Width', 'landingpress-wp' ),
		'section'  => 'landingpress_pagelayout',
		'default'  => 960,
		'choices'  => array(
			'min' => 960,
			'max' => 1200,
			'step' => 30,
		),
		'style'    => '.container, .site-header, .site-inner, .page-landingpress-boxed .site-inner, .page-landingpress-boxed-hf .site-inner { max-width: [value]px }',
	);

	$controls[] = array(
		'type'     => 'checkbox',
		'setting'  => 'landingpress_pagebuilder_elementor_disable',
		'label'    => esc_html__( 'DISABLE Elementor LandingPress', 'landingpress-wp' ),
		'description' => esc_html__( 'You can use this option when:', 'landingpress-wp' ).'<br/>'.esc_html__( '(1) you do not want to use Elementor', 'landingpress-wp' ).'<br/>'.esc_html__( '(2) you want to use the official version of Elementor', 'landingpress-wp' ),
		'section'  => 'landingpress_pagebuilder',
		'default'  => '',
	);

	$controls[] = array(
		'type'     => 'text',
		'setting'  => 'landingpress_facebook_pixel_id',
		'label'    => esc_html__( 'Facebook Pixel ID', 'landingpress-wp' ),
		'section'  => 'landingpress_facebook_pixel',
		'sanitize_callback' => 'landingpress_sanitize_nohtml',
	);

	for ($i=1; $i <= 5 ; $i++) { 
		$controls[] = array(
			'type'     => 'text',
			'setting'  => 'landingpress_facebook_pixel_id_'.$i,
			'label'    => esc_html__( 'Multiple Facebook Pixel ID #'.$i, 'landingpress-wp' ),
			'section'  => 'landingpress_facebook_pixel',
			'sanitize_callback' => 'landingpress_sanitize_nohtml',
		);
	}
	$controls[] = array(
		'type'     => 'text',
		'setting'  => 'landingpress_google_tag_manager_id',
		'label'    => esc_html__( 'Google Tag Manager ID', 'landingpress-wp' ),
		'description' => esc_html__( 'Put your Google Tag Manager ID here, for example: GTM-XXXXXX', 'landingpress-wp' ),
		'section'  => 'landingpress_scripts',
		'sanitize_callback' => 'landingpress_sanitize_nohtml',
	);

	$controls[] = array(
		'type'     => 'text',
		'setting'  => 'landingpress_google_analytics_id',
		'label'    => esc_html__( 'Google Analytics ID', 'landingpress-wp' ),
		'description' => esc_html__( 'Put your Google Analytics ID here, for example: UA-12345678-1', 'landingpress-wp' ),
		'section'  => 'landingpress_scripts',
		'sanitize_callback' => 'landingpress_sanitize_nohtml',
	);

	$controls[] = array(
		'type'     => 'text',
		'setting'  => 'landingpress_adwords_remarketing_id',
		'label'    => esc_html__( 'Adwords Remarketing - Google Conversion ID', 'landingpress-wp' ),
		'description' => esc_html__( 'Put your google_conversion_id from your Adwords Remarketing Code here', 'landingpress-wp' ),
		'section'  => 'landingpress_scripts',
		'sanitize_callback' => 'landingpress_sanitize_nohtml',
	);

	$controls[] = array(
		'type'     => 'color',
		'setting'  => 'landingpress_text_color',
		'label'    => esc_html__( 'Text Color', 'landingpress-wp' ),
		'section'  => 'colors',
		'default'  => '',
		'style'    => 'body, button, input, select, textarea { color: [value] }',
	);

	$controls[] = array(
		'type'     => 'color',
		'setting'  => 'landingpress_heading_color',
		'label'    => esc_html__( 'Heading Color', 'landingpress-wp' ),
		'section'  => 'colors',
		'default'  => '',
		'style'    => 'h1,h2,h3,h4,h5 { color: [value] }',
	);

	$controls[] = array(
		'type'     => 'color',
		'setting'  => 'landingpress_link_color',
		'label'    => esc_html__( 'Link Color', 'landingpress-wp' ),
		'section'  => 'colors',
		'default'  => '',
		'style'    => 'a, a:visited { color: [value] }',
	);

	$controls[] = array(
		'type'     => 'color',
		'setting'  => 'landingpress_link_color_hover',
		'label'    => esc_html__( 'Link Color (Hover)', 'landingpress-wp' ),
		'section'  => 'colors',
		'default'  => '',
		'style'    => 'a:hover { color: [value] }',
	);

	$controls[] = array(
		'type'     => 'color',
		'setting'  => 'landingpress_post_title_color',
		'label'    => esc_html__( 'Post Title - Text Color', 'landingpress-wp' ),
		'section'  => 'colors',
		'default'  => '',
		'style'    => '.entry .entry-title, .entry .entry-title a, .entry .entry-title a:visited { color: [value] }',
	);

	$controls[] = array(
		'type'     => 'color',
		'setting'  => 'landingpress_post_meta_color',
		'label'    => esc_html__( 'Post Meta - Text Color', 'landingpress-wp' ),
		'section'  => 'colors',
		'default'  => '',
		'style'    => '.entry-meta, .entry-meta a, .entry-meta a:visited { color: [value] }',
	);

	$controls[] = array(
		'type'     => 'color',
		'setting'  => 'landingpress_label_color',
		'label'    => esc_html__( 'Form Label - Text Color', 'landingpress-wp' ),
		'section'  => 'colors',
		'default'  => '',
		'style'    => 'label, .comment-form label { color: [value] }',
	);

	$controls[] = array(
		'type'     => 'color',
		'setting'  => 'landingpress_input_background',
		'label'    => esc_html__( 'Form Input/Textarea - Background Color', 'landingpress-wp' ),
		'section'  => 'colors',
		'default'  => '',
		'style'    => 'input[type="text"], input[type="email"], input[type="url"], input[type="password"], input[type="search"], textarea { background-color: [value] }',
	);

	$controls[] = array(
		'type'     => 'color',
		'setting'  => 'landingpress_input_color',
		'label'    => esc_html__( 'Form Input/Textarea - Text Color', 'landingpress-wp' ),
		'section'  => 'colors',
		'default'  => '',
		'style'    => 'input[type="text"], input[type="email"], input[type="url"], input[type="password"], input[type="search"], textarea { color: [value] }',
	);

	$controls[] = array(
		'type'     => 'color',
		'setting'  => 'landingpress_input_border',
		'label'    => esc_html__( 'Form Input/Textarea - Border Color', 'landingpress-wp' ),
		'section'  => 'colors',
		'default'  => '',
		'style'    => 'input[type="text"], input[type="email"], input[type="url"], input[type="password"], input[type="search"], textarea { border-color: [value] }',
	);

	$controls[] = array(
		'type'     => 'color',
		'setting'  => 'landingpress_button_background',
		'label'    => esc_html__( 'Form Button - Background Color', 'landingpress-wp' ),
		'section'  => 'colors',
		'default'  => '',
		'style'    => 'button, input[type=button], input[type=reset], input[type=submit] { background-color: [value] }',
	);

	$controls[] = array(
		'type'     => 'color',
		'setting'  => 'landingpress_button_color',
		'label'    => esc_html__( 'Form Button - Text Color', 'landingpress-wp' ),
		'section'  => 'colors',
		'default'  => '',
		'style'    => 'button, input[type=button], input[type=reset], input[type=submit] { color: [value] }',
	);

	$controls[] = array(
		'type'     => 'color',
		'setting'  => 'landingpress_button_border',
		'label'    => esc_html__( 'Form Button - Border Color', 'landingpress-wp' ),
		'section'  => 'colors',
		'default'  => '',
		'style'    => 'button, input[type=button], input[type=reset], input[type=submit] { border-color: [value] }',
	);

	$controls[] = array(
		'type'     => 'textarea',
		'setting'  => 'landingpress_script_header',
		'label'    => esc_html__( 'Header Script', 'landingpress-wp' ),
		'description' => esc_html__( 'Useful for third party scripts, for example Google Analytics, Histats, Facebook Pixel, etc', 'landingpress-wp' ),
		'section'  => 'landingpress_header_script',
		'sanitize_callback' => 'landingpress_sanitize_ads',
	);

	$controls[] = array(
		'type'     => 'checkbox',
		'setting'  => 'landingpress_header_hide',
		'label'    => esc_html__( 'Hide Header', 'landingpress-wp' ),
		'section'  => 'landingpress_header',
		'default'  => '',
		'priority' => 3,
	);

	$controls[] = array(
		'type'     => 'image',
		'setting'  => 'landingpress_header_logo',
		'label'    => esc_html__( 'Header Logo', 'landingpress-wp' ),
		'section'  => 'landingpress_header',
		'default'  => get_template_directory_uri().'/assets/images/logo.png',
		'priority' => 4,
	);

	$controls[] = array(
		'type'     => 'radio-buttonset',
		'setting'  => 'landingpress_header_alignment',
		'label'    => esc_html__( 'Header Alignment', 'landingpress-wp' ),
		'section'  => 'landingpress_header',
		'default'  => 'center',
		'choices'  => array(
			'left' => esc_html__( 'Left', 'landingpress-wp' ),
			'center' => esc_html__( 'Center', 'landingpress-wp' ),
			'right' => esc_html__( 'Right', 'landingpress-wp' ),
		),
		'style'  => array(
			'left' => '.site-branding{text-align:left;}',
			'center' => '.site-branding{text-align:center;}',
			'right' => '.site-branding{text-align:right;}',
		),
	);

	$controls[] = array(
		'type'     => 'radio',
		'setting'  => 'landingpress_header_placement',
		'label'    => esc_html__( 'Header Image Placement', 'landingpress-wp' ),
		'section'  => 'landingpress_header',
		'default'  => 'background',
		'choices'  => array(
			'background' => esc_html__( 'Background Image (Behind header logo)', 'landingpress-wp' ),
			'background_nologo' => esc_html__( 'Background Image (Without header logo)', 'landingpress-wp' ),
			'image' => esc_html__( 'Image Only (Instead of header logo)', 'landingpress-wp' ),
			'image_title_top' => esc_html__( 'Have header logo placed before the image', 'landingpress-wp' ),
			'image_title_bottom' => esc_html__( 'Have header logo placed after the image', 'landingpress-wp' ),
			// 'image_desc_bottom' => esc_html__( 'Have site description placed after the image', 'landingpress' ),
		),
		'priority' => 6,
	);

	$controls[] = array(
		'type'     => 'color',
		'setting'  => 'landingpress_header_background',
		'label'    => esc_html__( 'Header Background Color', 'landingpress-wp' ),
		'section'  => 'landingpress_header',
		'default'  => '',
		'style'    => '.site-branding { background-color: [value] }',
	);

	$controls[] = array(
		'type'     => 'slider',
		'setting'  => 'landingpress_header_paddingtop',
		'label'    => esc_html__( 'Header Top Padding', 'landingpress-wp' ),
		'section'  => 'landingpress_header',
		'default'  => 20,
		'choices'  => array(
			'min' => 0,
			'max' => 150,
			'step' => 5,
		),
		'style'    => '.site-branding { padding-top: [value]px }',
	);

	$controls[] = array(
		'type'     => 'slider',
		'setting'  => 'landingpress_header_paddingbottom',
		'label'    => esc_html__( 'Header Bottom Padding', 'landingpress-wp' ),
		'section'  => 'landingpress_header',
		'default'  => 20,
		'choices'  => array(
			'min' => 0,
			'max' => 150,
			'step' => 5,
		),
		'style'    => '.site-branding { padding-bottom: [value]px }',
	);

	$controls[] = array(
		'type'     => 'checkbox',
		'setting'  => 'landingpress_menu_sticky',
		'label'    => esc_html__( 'Sticky Menu', 'landingpress-wp' ),
		'section'  => 'landingpress_header_menu',
		'default'  => '1',
		'priority' => 5,
	);

	$controls[] = array(
		'type'     => 'image',
		'setting'  => 'landingpress_menu_logo',
		'label'    => esc_html__( 'Menu Logo', 'landingpress-wp' ),
		'section'  => 'landingpress_header_menu',
		'default'  => '',
		'priority' => 5,
	);

	$controls[] = array(
		'type'     => 'radio-buttonset',
		'setting'  => 'landingpress_menu_alignment',
		'label'    => esc_html__( 'Menu Alignment', 'landingpress-wp' ),
		'section'  => 'landingpress_header_menu',
		'default'  => 'left',
		'choices'  => array(
			'left' => esc_html__( 'Left', 'landingpress-wp' ),
			'center' => esc_html__( 'Center', 'landingpress-wp' ),
			'right' => esc_html__( 'Right', 'landingpress-wp' ),
		),
		'style'  => array(
			'left' => '',
			'center' => '.main-navigation {text-align:center; } .main-navigation ul.menu{ display:inline-block;vertical-align:top;}',
			'right' => '.main-navigation {text-align:right; } .main-navigation ul.menu{ display:inline-block;vertical-align:top;}',
		),
		'priority' => 5,
	);

	$controls[] = array(
		'type'     => 'checkbox',
		'setting'  => 'landingpress_menu_search',
		'label'    => esc_html__( 'Show search form', 'landingpress-wp' ),
		'section'  => 'landingpress_header_menu',
		'default'  => '1',
		'priority' => 5,
	);

	$controls[] = array(
		'type'     => 'slider',
		'setting'  => 'landingpress_menu_padding',
		'label'    => esc_html__( 'Menu Top & Bottom Padding', 'landingpress-wp' ),
		'section'  => 'landingpress_header_menu',
		'default'  => 0,
		'choices'  => array(
			'min' => 0,
			'max' => 100,
			'step' => 10,
		),
		'style'    => ' @media (min-width: 769px) { .main-navigation { padding-top:[value]px; padding-bottom:[value]px; } a.menu-logo img { margin: 0; height: 60px; } .is-sticky .main-navigation { padding-top:0; padding-bottom:0; } .is-sticky a.menu-logo img { margin: 12px 0 0; height: 40px; } } ',
	);

	$controls[] = array(
		'type'     => 'color',
		'setting'  => 'landingpress_menu_background',
		'label'    => esc_html__( 'Menu Background Color', 'landingpress-wp' ),
		'section'  => 'landingpress_header_menu',
		'default'  => '',
		'style'    => '.main-navigation, .main-navigation ul ul { background-color: [value] }',
	);

	$controls[] = array(
		'type'     => 'color',
		'setting'  => 'landingpress_menu_link_color',
		'label'    => esc_html__( 'Menu Link Color', 'landingpress-wp' ),
		'section'  => 'landingpress_header_menu',
		'default'  => '',
		'style'    => '.main-navigation li a, .main-navigation li a:visited, .menu-toggle, a.menu-minicart { color: [value] } .menu-bar { background : [value] } ',
	);

	$controls[] = array(
		'type'     => 'color',
		'setting'  => 'landingpress_menu_link_color_hover',
		'label'    => esc_html__( 'Menu Link Color (Hover)', 'landingpress-wp' ),
		'section'  => 'landingpress_header_menu',
		'default'  => '',
		'style'    => '.main-navigation li a:hover, a.menu-minicart:hover { color: [value] }',
	);

	$controls[] = array(
		'type'     => 'color',
		'setting'  => 'landingpress_menu_background_mobile',
		'label'    => esc_html__( '(Mobile) Menu Background Color', 'landingpress-wp' ),
		'section'  => 'landingpress_header_menu',
		'default'  => '',
		'style'    => ' @media (max-width: 768px) { .main-navigation .header-menu-container { background-color: [value] } }',
	);

	$controls[] = array(
		'type'     => 'color',
		'setting'  => 'landingpress_menu_link_color_mobile',
		'label'    => esc_html__( '(Mobile) Menu Link Color', 'landingpress-wp' ),
		'section'  => 'landingpress_header_menu',
		'default'  => '',
		'style'    => ' @media (max-width: 768px) { .main-navigation li a, .main-navigation li a:visited { color: [value] !important; } }',
	);

	$controls[] = array(
		'type'     => 'color',
		'setting'  => 'landingpress_menu_link_color_hover_mobile',
		'label'    => esc_html__( '(Mobile) Menu Link Color (Hover)', 'landingpress-wp' ),
		'section'  => 'landingpress_header_menu',
		'default'  => '',
		'style'    => ' @media (max-width: 768px) { .main-navigation li a:hover, a.menu-minicart:hover { color: [value] !important; } }',
	);

	$controls[] = array(
		'type'     => 'checkbox',
		'setting'  => 'landingpress_breadcrumb',
		'label'    => esc_html__( 'Show Breadcrumb', 'landingpress-wp' ),
		'section'  => 'landingpress_breadcrumb',
		'default'  => '1',
	);

	$controls[] = array(
		'type'     => 'color',
		'setting'  => 'landingpress_breadcrumb_color',
		'label'    => esc_html__( 'Breadcrumb Color', 'landingpress-wp' ),
		'section'  => 'landingpress_breadcrumb',
		'default'  => '',
		'style'    => '.breadcrumb { color: [value] }',
	);

	$controls[] = array(
		'type'     => 'color',
		'setting'  => 'landingpress_breadcrumb_link_color',
		'label'    => esc_html__( 'Breadcrumb Link Color', 'landingpress-wp' ),
		'section'  => 'landingpress_breadcrumb',
		'default'  => '',
		'style'    => '.breadcrumb a, .breadcrumb a:visited { color: [value] }',
	);

	$controls[] = array(
		'type'     => 'checkbox',
		'setting'  => 'landingpress_footer_widgets',
		'label'    => esc_html__( 'Show Footer Widgets', 'landingpress-wp' ),
		'section'  => 'landingpress_footer_widgets',
		'default'  => '1',
	);

	$controls[] = array(
		'type'     => 'color',
		'setting'  => 'landingpress_footer_widgets_background',
		'label'    => esc_html__( 'Footer Widgets Background Color', 'landingpress-wp' ),
		'section'  => 'landingpress_footer_widgets',
		'default'  => '',
		'style'    => '.site-footer-widgets { background-color: [value] }',
	);

	$controls[] = array(
		'type'     => 'color',
		'setting'  => 'landingpress_footer_widgets_border',
		'label'    => esc_html__( 'Footer Widgets Border Color', 'landingpress-wp' ),
		'section'  => 'landingpress_footer_widgets',
		'default'  => '',
		'style'    => '.site-footer-widgets { border-color: [value] }',
	);

	$controls[] = array(
		'type'     => 'color',
		'setting'  => 'landingpress_footer_widgets_title_color',
		'label'    => esc_html__( 'Footer Widgets Title Color', 'landingpress-wp' ),
		'section'  => 'landingpress_footer_widgets',
		'default'  => '',
		'style'    => '.footer-widget.widget .widget-title { color: [value] }',
	);

	$controls[] = array(
		'type'     => 'color',
		'setting'  => 'landingpress_footer_widgets_color',
		'label'    => esc_html__( 'Footer Widgets Text Color', 'landingpress-wp' ),
		'section'  => 'landingpress_footer_widgets',
		'default'  => '',
		'style'    => '.footer-widget.widget { color: [value] }',
	);

	$controls[] = array(
		'type'     => 'color',
		'setting'  => 'landingpress_footer_widgets_link_color',
		'label'    => esc_html__( 'Footer Widgets Link Color', 'landingpress-wp' ),
		'section'  => 'landingpress_footer_widgets',
		'default'  => '',
		'style'    => '.footer-widget.widget a, .footer-widget.widget a:visited { color: [value] }',
	);

	$controls[] = array(
		'type'     => 'color',
		'setting'  => 'landingpress_footer_widgets_link_color_hover',
		'label'    => esc_html__( 'Footer Widgets Link Color (Hover)', 'landingpress-wp' ),
		'section'  => 'landingpress_footer_widgets',
		'default'  => '',
		'style'    => '.footer-widget.widget a:hover { color: [value] }',
	);

	$controls[] = array(
		'type'     => 'color',
		'setting'  => 'landingpress_footer_widgets_line_color',
		'label'    => esc_html__( 'Footer Widgets Line Color', 'landingpress-wp' ),
		'section'  => 'landingpress_footer_widgets',
		'default'  => '',
		'style'    => '.footer-widget.widget .widget-title, .footer-widget.widget li, .footer-widget.widget ul ul { border-color: [value] }',
	);

	$controls[] = array(
		'type'     => 'checkbox',
		'setting'  => 'landingpress_footer_hide',
		'label'    => esc_html__( 'Hide Footer', 'landingpress-wp' ),
		'section'  => 'landingpress_footer',
		'default'  => '',
	);

	$controls[] = array(
		'type'     => 'textarea',
		'setting'  => 'landingpress_footer_text',
		'label'    => esc_html__( 'Footer Text', 'landingpress-wp' ),
		'section'  => 'landingpress_footer',
		'sanitize_callback' => 'wp_kses_post',
	);

	$controls[] = array(
		'type'     => 'color',
		'setting'  => 'landingpress_footer_background',
		'label'    => esc_html__( 'Footer Background Color', 'landingpress-wp' ),
		'section'  => 'landingpress_footer',
		'default'  => '',
		'style'    => '.site-footer .container { background-color: [value] } .site-footer-widgets { border-radius: 0; } .site-inner { border-bottom-right-radius: 0; border-bottom-left-radius: 0; }',
	);

	$controls[] = array(
		'type'     => 'color',
		'setting'  => 'landingpress_footer_text_color',
		'label'    => esc_html__( 'Footer Text Color', 'landingpress-wp' ),
		'section'  => 'landingpress_footer',
		'default'  => '',
		'style'    => '.site-footer { color: [value] }',
	);

	$controls[] = array(
		'type'     => 'color',
		'setting'  => 'landingpress_footer_link_color',
		'label'    => esc_html__( 'Footer Link Color', 'landingpress-wp' ),
		'section'  => 'landingpress_footer',
		'default'  => '',
		'style'    => '.site-footer a, .site-footer a:visited { color: [value] }',
	);

	$controls[] = array(
		'type'     => 'color',
		'setting'  => 'landingpress_footer_link_color_hover',
		'label'    => esc_html__( 'Footer Link Color (Hover)', 'landingpress-wp' ),
		'section'  => 'landingpress_footer',
		'default'  => '',
		'style'    => '.site-footer a:hover { color: [value] }',
	);

	$controls[] = array(
		'type'     => 'textarea',
		'setting'  => 'landingpress_script_footer',
		'label'    => esc_html__( 'Footer Script', 'landingpress-wp' ),
		'description' => esc_html__( 'Useful for third party scripts, for example Google Analytics, Histats, Facebook Pixel, etc', 'landingpress-wp' ),
		'section'  => 'landingpress_footer_script',
		'sanitize_callback' => 'landingpress_sanitize_ads',
	);

	$controls[] = array(
		'type'     => 'select',
		'setting'  => 'landingpress_archive_image',
		'label'    => esc_html__( 'Show featured image', 'landingpress-wp' ),
		'section'  => 'landingpress_template_archive',
		'default'  => 'featured',
		'choices'  => array(
			'featured' => esc_html__( 'Featured Image', 'landingpress-wp' ),
			'thumb-left' => esc_html__( 'Left Thumbnail', 'landingpress-wp' ),
			'thumb-right' => esc_html__( 'Right Thumbnail', 'landingpress-wp' ),
			'none' => esc_html__( 'No Image', 'landingpress-wp' ),
		),
	);

	$controls[] = array(
		'type'     => 'checkbox',
		'setting'  => 'landingpress_archive_meta',
		'label'    => esc_html__( 'Show post meta', 'landingpress-wp' ),
		'section'  => 'landingpress_template_archive',
		'default'  => '1',
	);

	$controls[] = array(
		'type'     => 'radio-buttonset',
		'setting'  => 'landingpress_archive_content',
		'label'    => esc_html__( 'Show content', 'landingpress-wp' ),
		'section'  => 'landingpress_template_archive',
		'default'  => 'content',
		'choices'  => array(
			'content' => esc_html__( 'Full Content', 'landingpress-wp' ),
			'excerpt' => esc_html__( 'Summary', 'landingpress-wp' ),
		),
	);

	$controls[] = array(
		'type'     => 'checkbox',
		'setting'  => 'landingpress_archive_morelink',
		'label'    => esc_html__( 'Show "Continue Reading" link on post summary', 'landingpress-wp' ),
		'section'  => 'landingpress_template_archive',
		'default'  => '1',
	);

	$controls[] = array(
		'type'     => 'text',
		'setting'  => 'landingpress_archive_morelink_text',
		'label'    => sprintf( esc_html__( 'Change Text: "%s"', 'landingpress-wp' ), esc_html__( 'Continue reading &rarr;', 'landingpress-wp' ) ),
		'section'  => 'landingpress_template_archive',
	);

	$controls[] = array(
		'type'     => 'color',
		'setting'  => 'landingpress_pagination_background',
		'label'    => esc_html__( 'Pagination Background', 'landingpress-wp' ),
		'section'  => 'landingpress_template_archive',
		'default'  => '',
		'style'    => '.posts-navigation li span, .posts-navigation li a, .page-links li span, .page-links li a { background-color: [value] }',
	);

	$controls[] = array(
		'type'     => 'color',
		'setting'  => 'landingpress_pagination_border',
		'label'    => esc_html__( 'Pagination Border', 'landingpress-wp' ),
		'section'  => 'landingpress_template_archive',
		'default'  => '',
		'style'    => '.posts-navigation li span, .posts-navigation li a, .page-links li span, .page-links li a { border-color: [value] }',
	);

	$controls[] = array(
		'type'     => 'color',
		'setting'  => 'landingpress_pagination_text_color',
		'label'    => esc_html__( 'Pagination Text Color', 'landingpress-wp' ),
		'section'  => 'landingpress_template_archive',
		'default'  => '',
		'style'    => '.posts-navigation li span, .page-links li span { color: [value] }',
	);

	$controls[] = array(
		'type'     => 'color',
		'setting'  => 'landingpress_pagination_link_color',
		'label'    => esc_html__( 'Pagination Link Color', 'landingpress-wp' ),
		'section'  => 'landingpress_template_archive',
		'default'  => '',
		'style'    => '.posts-navigation li a, .page-links li a { color: [value] }',
	);

	$controls[] = array(
		'type'     => 'checkbox',
		'setting'  => 'landingpress_post_image',
		'label'    => esc_html__( 'Show featured image', 'landingpress-wp' ),
		'section'  => 'landingpress_template_post',
		'default'  => '1',
	);

	$controls[] = array(
		'type'     => 'checkbox',
		'setting'  => 'landingpress_post_share_before',
		'label'    => esc_html__( 'Show social share before content', 'landingpress-wp' ),
		'section'  => 'landingpress_template_post',
		'default'  => '',
	);

	$controls[] = array(
		'type'     => 'checkbox',
		'setting'  => 'landingpress_post_share_after',
		'label'    => esc_html__( 'Show social share after content', 'landingpress-wp' ),
		'section'  => 'landingpress_template_post',
		'default'  => '1',
	);

	$controls[] = array(
		'type'     => 'checkbox',
		'setting'  => 'landingpress_post_related',
		'label'    => esc_html__( 'Show related posts', 'landingpress-wp' ),
		'section'  => 'landingpress_template_post',
		'default'  => '1',
	);

	$controls[] = array(
		'type'     => 'checkbox',
		'setting'  => 'landingpress_post_related_image',
		'label'    => esc_html__( 'Show related post images', 'landingpress-wp' ),
		'section'  => 'landingpress_template_post',
		'default'  => '1',
	);

	$controls[] = array(
		'type'     => 'checkbox',
		'setting'  => 'landingpress_post_comments',
		'label'    => esc_html__( 'Show comments', 'landingpress-wp' ),
		'section'  => 'landingpress_template_post',
		'default'  => '1',
	);

	$controls[] = array(
		'type'     => 'color',
		'setting'  => 'landingpress_post_share_label_color',
		'label'    => esc_html__( 'Social Share Label Color', 'landingpress-wp' ),
		'section'  => 'landingpress_template_post',
		'default'  => '',
		'style'    => '.share-label { color: [value]; border-color: [value] }',
	);

	$controls[] = array(
		'type'     => 'color',
		'setting'  => 'landingpress_post_related_separator_color',
		'label'    => esc_html__( 'Related Post Separator Color', 'landingpress-wp' ),
		'section'  => 'landingpress_template_post',
		'default'  => '',
		'style'    => '.related-posts li { border-color: [value] }',
	);

	$controls[] = array(
		'type'     => 'checkbox',
		'setting'  => 'landingpress_page_share_before',
		'label'    => esc_html__( 'Show social share before content', 'landingpress-wp' ),
		'section'  => 'landingpress_template_page',
		'default'  => '',
	);

	$controls[] = array(
		'type'     => 'checkbox',
		'setting'  => 'landingpress_page_share_after',
		'label'    => esc_html__( 'Show social share after content', 'landingpress-wp' ),
		'section'  => 'landingpress_template_page',
		'default'  => '',
	);

	$controls[] = array(
		'type'     => 'checkbox',
		'setting'  => 'landingpress_page_comments',
		'label'    => esc_html__( 'Show comments', 'landingpress-wp' ),
		'section'  => 'landingpress_template_page',
		'default'  => '1',
	);

	$controls[] = array(
		'type'     => 'checkbox',
		'setting'  => 'landingpress_attachment_share_before',
		'label'    => esc_html__( 'Show social share before content', 'landingpress-wp' ),
		'section'  => 'landingpress_template_attachment',
		'default'  => '',
	);

	$controls[] = array(
		'type'     => 'checkbox',
		'setting'  => 'landingpress_attachment_share_after',
		'label'    => esc_html__( 'Show social share after content', 'landingpress-wp' ),
		'section'  => 'landingpress_template_attachment',
		'default'  => '',
	);

	$controls[] = array(
		'type'     => 'checkbox',
		'setting'  => 'landingpress_attachment_comments',
		'label'    => esc_html__( 'Show comments', 'landingpress-wp' ),
		'section'  => 'landingpress_template_attachment',
		'default'  => '1',
	);

	$controls[] = array(
		'type'     => 'text',
		'setting'  => 'landingpress_comment_fb_app_id',
		'label'    => esc_html__( 'FB App ID for Facebook Comment', 'landingpress-wp' ),
		'section'  => 'landingpress_template_comments',
	);

	$controls[] = array(
		'type'     => 'checkbox',
		'setting'  => 'landingpress_comment_hide_default',
		'label'    => esc_html__( 'Hide default WordPress comments. Use it if you want to use Facebook comments only.', 'landingpress-wp' ),
		'section'  => 'landingpress_template_comments',
		'default'  => '',
	);

	$controls[] = array(
		'type'     => 'checkbox',
		'setting'  => 'landingpress_comment_textarea_reverse',
		'label'    => esc_html__( 'Reverse Comment Textarea To The Bottom', 'landingpress-wp' ),
		'section'  => 'landingpress_template_comments',
		'default'  => '',
	);

	$controls[] = array(
		'type'     => 'checkbox',
		'setting'  => 'landingpress_comment_url_hide',
		'label'    => esc_html__( 'Hide Comment URL (Website) Field', 'landingpress-wp' ),
		'section'  => 'landingpress_template_comments',
		'default'  => '',
	);

	$controls[] = array(
		'type'     => 'text',
		'setting'  => 'landingpress_comment_form_name',
		'label'    => sprintf( esc_html__( 'Change Text: "%s"', 'landingpress-wp' ), esc_html__( 'Your Name', 'landingpress-wp' ) ),
		'section'  => 'landingpress_template_comments',
	);

	$controls[] = array(
		'type'     => 'text',
		'setting'  => 'landingpress_comment_form_email',
		'label'    => sprintf( esc_html__( 'Change Text: "%s"', 'landingpress-wp' ), esc_html__( 'Your Email', 'landingpress-wp' ) ),
		'section'  => 'landingpress_template_comments',
	);

	$controls[] = array(
		'type'     => 'text',
		'setting'  => 'landingpress_comment_form_website',
		'label'    => sprintf( esc_html__( 'Change Text: "%s"', 'landingpress-wp' ), esc_html__( 'Your Website', 'landingpress-wp' ) ),
		'section'  => 'landingpress_template_comments',
	);

	$controls[] = array(
		'type'     => 'text',
		'setting'  => 'landingpress_comment_form_comment',
		'label'    => sprintf( esc_html__( 'Change Text: "%s"', 'landingpress-wp' ), esc_html__( 'Your Comment', 'landingpress-wp' ) ),
		'section'  => 'landingpress_template_comments',
	);

	$controls[] = array(
		'type'     => 'text',
		'setting'  => 'landingpress_comment_form_notes',
		'label'    => sprintf( esc_html__( 'Change Text: "%s"', 'landingpress-wp' ), esc_html__( 'Your email address will not be published.', 'landingpress-wp' ) ),
		'section'  => 'landingpress_template_comments',
	);

	$controls[] = array(
		'type'     => 'text',
		'setting'  => 'landingpress_comment_form_required',
		'label'    => sprintf( esc_html__( 'Change Text: "%s"', 'landingpress-wp' ), esc_html__( 'Required fields are marked %s', 'landingpress-wp' ) ),
		'section'  => 'landingpress_template_comments',
	);

	$controls[] = array(
		'type'     => 'text',
		'setting'  => 'landingpress_comment_form_title_reply',
		'label'    => sprintf( esc_html__( 'Change Text: "%s"', 'landingpress-wp' ), esc_html__( 'Leave a Reply', 'landingpress-wp' ) ),
		'section'  => 'landingpress_template_comments',
	);

	$controls[] = array(
		'type'     => 'text',
		'setting'  => 'landingpress_comment_form_title_reply_to',
		'label'    => sprintf( esc_html__( 'Change Text: "%s"', 'landingpress-wp' ), esc_html__( 'Leave a Reply to %s', 'landingpress-wp' ) ),
		'section'  => 'landingpress_template_comments',
	);

	$controls[] = array(
		'type'     => 'text',
		'setting'  => 'landingpress_comment_form_cancel_reply_link',
		'label'    => sprintf( esc_html__( 'Change Text: "%s"', 'landingpress-wp' ), esc_html__( 'Cancel reply', 'landingpress-wp' ) ),
		'section'  => 'landingpress_template_comments',
	);

	$controls[] = array(
		'type'     => 'text',
		'setting'  => 'landingpress_comment_form_label_submit',
		'label'    => sprintf( esc_html__( 'Change Text: "%s"', 'landingpress-wp' ), esc_html__( 'Post Comment', 'landingpress-wp' ) ),
		'section'  => 'landingpress_template_comments',
	);

	$controls[] = array(
		'type'     => 'color',
		'setting'  => 'landingpress_post_comments_box_border',
		'label'    => esc_html__( 'Comment Box Border Color', 'landingpress-wp' ),
		'section'  => 'landingpress_template_comments',
		'default'  => '',
		'style'    => '.comment-body { border-color: [value] }',
	);

	$controls[] = array(
		'type'     => 'radio-buttonset',
		'setting'  => 'landingpress_sidebar_position',
		'label'    => esc_html__( 'Sidebar Position', 'landingpress-wp' ),
		'section'  => 'landingpress_template_sidebar',
		'default'  => 'right',
		'choices'  => array(
			'right' => esc_html__( 'Right', 'landingpress-wp' ),
			'left' => esc_html__( 'Left', 'landingpress-wp' ),
		),
		'style'  => array(
			'right' => '',
			'left' => '#primary{float:right;}#secondary{float:left;}',
		),
	);

	$controls[] = array(
		'type'     => 'color',
		'setting'  => 'landingpress_sidebar_widgets_title_color',
		'label'    => esc_html__( 'Sidebar Widgets Title Color', 'landingpress-wp' ),
		'section'  => 'landingpress_template_sidebar',
		'default'  => '',
		'style'    => '.widget .widget-title { color: [value] }',
	);

	$controls[] = array(
		'type'     => 'color',
		'setting'  => 'landingpress_sidebar_widgets_color',
		'label'    => esc_html__( 'Sidebar Widgets Text Color', 'landingpress-wp' ),
		'section'  => 'landingpress_template_sidebar',
		'default'  => '',
		'style'    => '.widget { color: [value] }',
	);

	$controls[] = array(
		'type'     => 'color',
		'setting'  => 'landingpress_sidebar_widgets_link_color',
		'label'    => esc_html__( 'Sidebar Widgets Link Color', 'landingpress-wp' ),
		'section'  => 'landingpress_template_sidebar',
		'default'  => '',
		'style'    => '.widget a, .widget a:visited { color: [value] }',
	);

	$controls[] = array(
		'type'     => 'color',
		'setting'  => 'landingpress_sidebar_widgets_link_color_hover',
		'label'    => esc_html__( 'Sidebar Widgets Link Color (Hover)', 'landingpress-wp' ),
		'section'  => 'landingpress_template_sidebar',
		'default'  => '',
		'style'    => '.widget a:hover { color: [value] }',
	);

	$controls[] = array(
		'type'     => 'color',
		'setting'  => 'landingpress_sidebar_widgets_line_color',
		'label'    => esc_html__( 'Sidebar Widgets Line Color', 'landingpress-wp' ),
		'section'  => 'landingpress_template_sidebar',
		'default'  => '',
		'style'    => '.widget .widget-title, .widget li, .widget ul ul { border-color: [value] }',
	);

	$controls[] = array(
		'type'     => 'checkbox',
		'setting'  => 'landingpress_optimization_jquery',
		'label'    => esc_html__( 'Force jquery from head to bottom', 'landingpress-wp' ),
		'section'  => 'landingpress_optimization',
		'default'  => '',
	);

	$controls[] = array(
		'type'     => 'checkbox',
		'setting'  => 'landingpress_optimization_defer',
		'label'    => esc_html__( 'Defer parsing of JavaScript', 'landingpress-wp' ),
		'section'  => 'landingpress_optimization',
		'default'  => '1',
	);

	$controls[] = array(
		'type'     => 'checkbox',
		'setting'  => 'landingpress_optimization_xmlrpc',
		'label'    => esc_html__( 'Enable XMLRPC support', 'landingpress-wp' ),
		'section'  => 'landingpress_optimization',
		'default'  => '',
	);

	$controls[] = array(
		'type'     => 'checkbox',
		'setting'  => 'landingpress_optimization_wlw',
		'label'    => esc_html__( 'Enable Windows Live Writer support', 'landingpress-wp' ),
		'section'  => 'landingpress_optimization',
		'default'  => '',
	);

	// $controls[] = array(
	// 	'type'     => 'checkbox',
	// 	'setting'  => 'landingpress_optimization_version',
	// 	'label'    => esc_html__( 'Remove query strings from JavaScript & CSS', 'landingpress' ),
	// 	'section'  => 'landingpress_optimization',
	// 	'default'  => '',
	// );

	$controls[] = array(
		'type'     => 'textarea',
		'setting'  => 'landingpress_ad_site_content_before',
		'label'    => esc_html__( 'Ad - Before site content', 'landingpress-wp' ),
		'section'  => 'landingpress_ads_sitewide',
		'sanitize_callback' => 'landingpress_sanitize_ads',
	);

	$controls[] = array(
		'type'     => 'textarea',
		'setting'  => 'landingpress_ad_site_content_after',
		'label'    => esc_html__( 'Ad - After site content', 'landingpress-wp' ),
		'section'  => 'landingpress_ads_sitewide',
		'sanitize_callback' => 'landingpress_sanitize_ads',
	);

	$controls[] = array(
		'type'     => 'textarea',
		'setting'  => 'landingpress_ad_archive_row1_after',
		'label'    => esc_html__( 'Ad - After first post row', 'landingpress-wp' ),
		'section'  => 'landingpress_ads_archive',
		'sanitize_callback' => 'landingpress_sanitize_ads',
	);

	$controls[] = array(
		'type'     => 'textarea',
		'setting'  => 'landingpress_ad_archive_row2_after',
		'label'    => esc_html__( 'Ad - After second post row', 'landingpress-wp' ),
		'section'  => 'landingpress_ads_archive',
		'sanitize_callback' => 'landingpress_sanitize_ads',
	);

	$controls[] = array(
		'type'     => 'textarea',
		'setting'  => 'landingpress_ad_archive_row3_after',
		'label'    => esc_html__( 'Ad - After third post row', 'landingpress-wp' ),
		'section'  => 'landingpress_ads_archive',
		'sanitize_callback' => 'landingpress_sanitize_ads',
	);

	$controls[] = array(
		'type'     => 'textarea',
		'setting'  => 'landingpress_ad_post_content_before',
		'label'    => esc_html__( 'Ad - Before post content', 'landingpress-wp' ),
		'section'  => 'landingpress_ads_post',
		'sanitize_callback' => 'landingpress_sanitize_ads',
	);

	$controls[] = array(
		'type'     => 'textarea',
		'setting'  => 'landingpress_ad_post_content_after',
		'label'    => esc_html__( 'Ad - After post content', 'landingpress-wp' ),
		'section'  => 'landingpress_ads_post',
		'sanitize_callback' => 'landingpress_sanitize_ads',
	);

	return $controls;
}

// add_filter( 'landingpress_customize_controls', 'landingpress_customize_controls_customcss', 999 );
function landingpress_customize_controls_customcss( $controls ) {

	$controls[] = array(
		'type'     => 'custom-css',
		'setting'  => 'landingpress_customcss',
		'label'    => esc_html__( 'Custom CSS', 'landingpress-wp' ),
		'section'  => 'landingpress_customcss',
		'default'  => '',
		'sanitize_callback' => 'landingpress_sanitize_css',
	);

	return $controls;
}

add_filter( 'landingpress_fonts_customize_controls', 'landingpress_fonts_customize_controls' );
function landingpress_fonts_customize_controls( $controls ) {

	if ( get_option( LANDINGPRESS_THEME_SLUG . '_license_key_status', false) != 'valid' ) {
		return $controls;
	}
	
	$controls[] = array(
		'setting'  => 'landingpress_font_body',
		'label'    => esc_html__( 'Body Font', 'landingpress-wp' ),
		'section'  => 'landingpress_fonts',
		'selector' => 'body,.site-description',
	);

	$controls[] = array(
		'setting'  => 'landingpress_font_heading',
		'label'    => esc_html__( 'Heading Font', 'landingpress-wp' ),
		'section'  => 'landingpress_fonts',
		'selector' => 'h1,h2,h3,h4,h5,h6,.site-title',
	);

	return $controls;
}

