<?php 

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

function landingpress_sanitize_checkbox( $checked ) {
	return ( ( isset( $checked ) && true == $checked ) ? true : false );
}

function landingpress_sanitize_css( $css ) {
	return wp_strip_all_tags( $css );
}

function landingpress_sanitize_dropdown_pages( $page_id, $setting ) {
	$page_id = absint( $page_id );
	return ( 'publish' == get_post_status( $page_id ) ? $page_id : $setting->default );
}

function landingpress_sanitize_email( $email, $setting ) {
	$email = sanitize_email( $email );
	return ( ! null( $email ) ? $email : $setting->default );
}

function landingpress_sanitize_hex_color( $hex_color, $setting ) {
	$hex_color = sanitize_hex_color( $hex_color );
	return ( ! null( $hex_color ) ? $hex_color : $setting->default );
}

function landingpress_sanitize_html( $html ) {
	return wp_filter_post_kses( $html );
}

function landingpress_sanitize_image( $image, $setting ) {
    $mimes = array(
        'jpg|jpeg|jpe' => 'image/jpeg',
        'gif'          => 'image/gif',
        'png'          => 'image/png',
        'bmp'          => 'image/bmp',
        'tif|tiff'     => 'image/tiff',
        'ico'          => 'image/x-icon'
    );
    $file = wp_check_filetype( $image, $mimes );
    return ( $file['ext'] ? $image : $setting->default );
}

function landingpress_sanitize_nohtml( $nohtml ) {
	return wp_filter_nohtml_kses( $nohtml );
}

function landingpress_sanitize_number_absint( $number, $setting ) {
	$number = absint( $number );
	return ( $number ? $number : $setting->default );
}

function landingpress_sanitize_number_range( $number, $setting ) {
	$number = absint( $number );
	$atts = $setting->manager->get_control( $setting->id )->input_attrs;
	$min = ( isset( $atts['min'] ) ? $atts['min'] : $number );
	$max = ( isset( $atts['max'] ) ? $atts['max'] : $number );
	$step = ( isset( $atts['step'] ) ? $atts['step'] : 1 );
	return ( $min <= $number && $number <= $max && is_int( $number / $step ) ? $number : $setting->default );
}

function landingpress_sanitize_select( $input, $setting ) {
	$input = sanitize_key( $input );
	$choices = $setting->manager->get_control( $setting->id )->choices;
	return ( array_key_exists( $input, $choices ) ? $input : $setting->default );
}

function landingpress_sanitize_url( $url ) {
	return esc_url_raw( $url );
}

function landingpress_sanitize_ads( $html ) {
	// if ( current_user_can('unfiltered_html') )
	// 	return $html;
	// else
	// 	return wp_filter_post_kses( $html );
	return $html;
}

add_action( 'customize_register', 'landingpress_customize_register', 10 );
/**
 * This function incorporates code from the Kirki Customizer Framework.
 * 
 * The Kirki Customizer Framework, Copyright Aristeides Stathopoulos (@aristath),
 * is licensed under the terms of the GNU GPL, Version 2 (or later).
 * 
 * @link http://kirki.org
 */
function landingpress_customize_register( $wp_customize ){

	if ( ! isset( $wp_customize ) ) {
		return;
	}

	class LandingPress_Customize_Slider_Control extends WP_Customize_Control {

		public $type = 'slider';

		public function enqueue() {

			wp_enqueue_script( 'jquery-ui' );
			wp_enqueue_script( 'jquery-ui-slider' );

		}

		public function render_content() { ?>
			<label>

				<span class="customize-control-title">
					<?php echo esc_attr( $this->label ); ?>
					<?php if ( ! empty( $this->description ) ) : ?>
						<span class="description customize-control-description"><?php echo esc_html( $this->description ); ?></span>
					<?php endif; ?>
				</span>

				<input type="text" class="" id="input_<?php echo esc_attr( $this->id ); ?>" value="<?php echo esc_attr( $this->value() ); ?>" <?php $this->link(); ?>/>

			</label>

			<div id="slider_<?php echo esc_attr( $this->id ); ?>" class="ss-slider"></div>
			<script>
			jQuery(document).ready(function($) {
				$( '[id="slider_<?php echo esc_attr( $this->id ); ?>"]' ).slider({
						value : <?php echo esc_attr( $this->value() ); ?>,
						min   : <?php echo esc_attr( $this->choices['min'] ); ?>,
						max   : <?php echo esc_attr( $this->choices['max'] ); ?>,
						step  : <?php echo esc_attr( $this->choices['step'] ); ?>,
						slide : function( event, ui ) { 
							$( '[id="input_<?php echo esc_attr( $this->id ); ?>"]' ).val(ui.value).trigger('change'); 
						}
				});
				$( '[id="input_<?php echo esc_attr( $this->id ); ?>"]' ).val( $( '[id="slider_<?php echo esc_attr( $this->id ); ?>"]' ).slider( "value" ) );

				$( '[id="input_<?php echo esc_attr( $this->id ); ?>"]' ).on( 'change keyup paste', function() {
					$( '[id="slider_<?php echo esc_attr( $this->id ); ?>"]' ).slider({
						value : $( this ).val()
					});
				});

			});
			</script>
			<?php

		}
	}
	
	class LandingPress_Customize_Radio_Buttonset_Control extends WP_Customize_Control {

		public $type = 'radio-buttonset';

		public function enqueue() {
			wp_enqueue_script( 'jquery-ui-button' );
		}

		public function render_content() {

			if ( empty( $this->choices ) ) {
				return;
			}

			$name = '_customize-radio-'.$this->id;

			?>
			<span class="customize-control-title">
				<?php echo esc_attr( $this->label ); ?>
				<?php if ( ! empty( $this->description ) ) : ?>
					<span class="description customize-control-description"><?php echo esc_html( $this->description ); ?></span>
				<?php endif; ?>
			</span>

			<div id="input_<?php echo esc_attr( $this->id ); ?>" class="buttonset">
				<?php foreach ( $this->choices as $value => $label ) : ?>
					<input type="radio" value="<?php echo esc_attr( $value ); ?>" name="<?php echo esc_attr( $name ); ?>" id="<?php echo esc_attr( $this->id ).esc_attr( $value ); ?>" <?php $this->link(); checked( $this->value(), $value ); ?>>
						<label for="<?php echo esc_attr( $this->id ).esc_attr( $value ); ?>">
							<?php echo esc_html( $label ); ?>
						</label>
					</input>
				<?php endforeach; ?>
			</div>
			<script>jQuery(document).ready(function($) { $( '[id="input_<?php echo esc_attr( $this->id ); ?>"]' ).buttonset(); });</script>
			<?php
		}

	}

	class LandingPress_Customize_Radio_Image_Control extends WP_Customize_Control {

		public $type = 'radio-image';

		public function enqueue() {
			wp_enqueue_script( 'jquery-ui-button' );
		}

		public function render_content() {

			if ( empty( $this->choices ) ) {
				return;
			}

			$name = '_customize-radio-'.$this->id;

			?>
			<span class="customize-control-title">
				<?php echo esc_attr( $this->label ); ?>
				<?php if ( ! empty( $this->description ) ) : ?>
					<span class="description customize-control-description"><?php echo esc_html( $this->description ); ?></span>
				<?php endif; ?>
			</span>
			<div id="input_<?php echo esc_attr( $this->id ); ?>" class="image">
				<?php foreach ( $this->choices as $value => $label ) : ?>
					<input class="image-select" type="radio" value="<?php echo esc_attr( $value ); ?>" name="<?php echo esc_attr( $name ); ?>" id="<?php echo esc_attr( $this->id ).esc_attr( $value ); ?>" <?php $this->link(); checked( $this->value(), $value ); ?>>
						<label for="<?php echo esc_attr( $this->id ).esc_attr( $value ); ?>">
							<img src="<?php echo esc_html( $label ); ?>">
						</label>
					</input>
				<?php endforeach; ?>
			</div>
			<script>jQuery(document).ready(function($) { $( '[id="input_<?php echo esc_attr( $this->id ); ?>"]' ).buttonset(); });</script>
			<?php
		}

	}

	class LandingPress_Customize_Dropdown_Categories_Control extends WP_Customize_Control {

		public $type = 'dropdown-categories';

		public function render_content() {
			?>
			<label>
			<?php if ( ! empty( $this->label ) ) : ?>
				<span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
			<?php endif;
			if ( ! empty( $this->description ) ) : ?>
				<span class="description customize-control-description"><?php echo esc_html( $this->description ); ?></span>
			<?php endif; ?>

			<?php $dropdown = wp_dropdown_categories(
				array(
					'name'              => '_customize-dropdown-categories-' . $this->id,
					'echo'              => 0,
					'show_option_none'  => esc_html__( '&mdash; Select &mdash;', 'landingpress-wp' ),
					'option_none_value' => '0',
					'selected'          => $this->value(),
				)
			);

			// Hackily add in the data link parameter.
			echo str_replace( '<select', '<select ' . $this->get_link(), $dropdown );
			?>
			</label>
			<?php
		}

	}

	class LandingPress_Customize_Custom_CSS_Control extends WP_Customize_Control {

		public $type = 'custom-css';

		public function render_content() {
			?>
			<label>
				<?php if ( ! empty( $this->label ) ) : ?>
					<span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
				<?php endif;
				if ( ! empty( $this->description ) ) : ?>
					<span class="description customize-control-description"><?php echo esc_html( $this->description ); ?></span>
				<?php endif; ?>
				<textarea rows="20" <?php $this->link(); ?> style="width:100%;resize:vertical;">
					<?php echo esc_textarea( $this->value() ); ?>
				</textarea>
			</label>
			<?php
		}

	}

	$fields = apply_filters( 'landingpress_customize_controls', array() );

	if ( ! empty( $fields ) ) {
		foreach ( $fields as $field ) {

			$defaults = array(
				'setting'				=> '',
				'section'				=> '',
				'type'					=> '',
				'priority'				=> 10,
				'label'					=> '',
				'description'			=> '',
				'choices'				=> array(),
				'input_attrs'			=> array(),
				'default'				=> '',
				'capability'			=> 'edit_theme_options',
				'sanitize_callback'		=> '',
			);

			$field = wp_parse_args( $field, $defaults );

			if ( $field['setting'] && $field['section'] && $field['type'] ) {
				$wp_customize->add_setting(
					$field['setting'],
					array(
						'default'			=> $field['default'],
						'type'				=> 'theme_mod',
						'capability'		=> $field['capability'],
						'sanitize_callback'	=> $field['sanitize_callback'],
					)
				);
				if ( in_array( $field['type'], array( 'checkbox', 'dropdown-pages', 'text', 'textarea', 'email', 'number', 'tel', 'url' ) ) ) {
					$wp_customize->add_control(
						$field['setting'],
						array(
							'settings'		=> $field['setting'],
							'section'		=> $field['section'],
							'type'			=> $field['type'],
							'priority'		=> $field['priority'],
							'label'			=> $field['label'],
							'description'	=> $field['description'],
						)
					);
				}
				elseif ( in_array( $field['type'], array( 'radio', 'select' ) ) && !empty( $field['choices'] ) ) {
					$wp_customize->add_control(
						$field['setting'],
						array(
							'settings'		=> $field['setting'],
							'section'		=> $field['section'],
							'type'			=> $field['type'],
							'priority'		=> $field['priority'],
							'label'			=> $field['label'],
							'description'	=> $field['description'],
							'choices'		=> $field['choices'],
						)
					);
				}
				elseif ( in_array( $field['type'], array( 'radio', 'select' ) ) && !empty( $field['choices'] ) ) {
					$wp_customize->add_control(
						$field['setting'],
						array(
							'settings'		=> $field['setting'],
							'section'		=> $field['section'],
							'type'			=> $field['type'],
							'priority'		=> $field['priority'],
							'label'			=> $field['label'],
							'description'	=> $field['description'],
							'choices'		=> $field['choices'],
						)
					);
				}
				elseif ( 'color' == $field['type'] ) {
					$wp_customize->add_control(
						new WP_Customize_Color_Control( 
							$wp_customize,
							$field['setting'],
							array(
								'settings'		=> $field['setting'],
								'section'		=> $field['section'],
								'priority'		=> $field['priority'],
								'label'			=> $field['label'],
								'description'	=> $field['description'],
							)
						)
					);
				}
				elseif ( 'image' == $field['type'] ) {
					$wp_customize->add_control(
						new WP_Customize_Image_Control( 
							$wp_customize,
							$field['setting'],
							array(
								'settings'		=> $field['setting'],
								'section'		=> $field['section'],
								'priority'		=> $field['priority'],
								'label'			=> $field['label'],
								'description'	=> $field['description'],
							)
						)
					);
				}
				elseif ( 'slider' == $field['type'] ) {
					$wp_customize->add_control(
						new LandingPress_Customize_Slider_Control( 
							$wp_customize,
							$field['setting'],
							array(
								'settings'		=> $field['setting'],
								'section'		=> $field['section'],
								'priority'		=> $field['priority'],
								'label'			=> $field['label'],
								'description'	=> $field['description'],
								'choices'		=> $field['choices'],
							)
						)
					);
				}
				elseif ( 'radio-buttonset' == $field['type'] ) {
					$wp_customize->add_control(
						new LandingPress_Customize_Radio_Buttonset_Control( 
							$wp_customize,
							$field['setting'],
							array(
								'settings'		=> $field['setting'],
								'section'		=> $field['section'],
								'priority'		=> $field['priority'],
								'label'			=> $field['label'],
								'description'	=> $field['description'],
								'choices'		=> $field['choices'],
							)
						)
					);
				}
				elseif ( 'radio-image' == $field['type'] ) {
					$wp_customize->add_control(
						new LandingPress_Customize_Radio_Image_Control( 
							$wp_customize,
							$field['setting'],
							array(
								'settings'		=> $field['setting'],
								'section'		=> $field['section'],
								'priority'		=> $field['priority'],
								'label'			=> $field['label'],
								'description'	=> $field['description'],
								'choices'		=> $field['choices'],
							)
						)
					);
				}
				elseif ( 'dropdown-categories' == $field['type'] ) {
					$wp_customize->add_control(
						new LandingPress_Customize_Dropdown_Categories_Control( 
							$wp_customize,
							$field['setting'],
							array(
								'settings'		=> $field['setting'],
								'section'		=> $field['section'],
								'priority'		=> $field['priority'],
								'label'			=> $field['label'],
								'description'	=> $field['description'],
								'choices'		=> $field['choices'],
							)
						)
					);
				}
				elseif ( 'custom-css' == $field['type'] ) {
					$wp_customize->add_control(
						new LandingPress_Customize_Custom_CSS_Control( 
							$wp_customize,
							$field['setting'],
							array(
								'settings'		=> $field['setting'],
								'section'		=> $field['section'],
								'priority'		=> $field['priority'],
								'label'			=> $field['label'],
								'description'	=> $field['description'],
								'choices'		=> $field['choices'],
							)
						)
					);
				}
			}
		}
	}
}

add_action( 'customize_controls_print_styles', 'landingpress_customize_controls_print_styles' );
/**
 * This function incorporates CSS from the Kirki Customizer Framework.
 * 
 * The Kirki Customizer Framework, Copyright Aristeides Stathopoulos (@aristath),
 * is licensed under the terms of the GNU GPL, Version 2 (or later).
 * 
 * @link http://kirki.org
 */
function landingpress_customize_controls_print_styles() { 
?>
<style>
.customize-control-slider input[type="text"] {
	background: none;
	border: none;
	text-align: center;
	padding: 0;
	margin: 0;
	font-size: 12px;
	box-shadow: none;
	color: #333;
	width: 100%;
}
.customize-control-slider .ui-slider {
	position: relative;
	text-align: left;
	height: 7px;
	border-radius: 3px;
	background: #f7f7f7;
	border: 1px solid #ccc;
	box-shadow: 0 1px 0 #ccc;
	margin-top: 10px;
	margin-bottom: 20px; 
}
.customize-control-slider .ui-slider .ui-slider-handle {
	position: absolute;
	z-index: 2;
	width: 15px;
	height: 15px;
	top: -5px;
	border-radius: 50%;
	cursor: default;
	-ms-touch-action: none;
	touch-action: none;
	background: #555;
	border: 1px solid #555;
	margin-left: -8px;
}
.customize-control-slider .ui-slider .ui-slider-range {
	position: absolute;
	z-index: 1;
	font-size: 0.7em;
	display: block;
	border: 0;
	background-position: 0 0; 
}
.customize-control-radio-buttonset label {
	padding: 5px 10px;
	background: #f7f7f7;
	border: 1px solid #ccc;
	border-left: 0;
	line-height: 35px;
	box-shadow: 0 1px 0 #ccc;
}
.customize-control-radio-buttonset label.ui-state-active {
	background: #555;
	color: #fff;
}
.customize-control-radio-buttonset label.ui-corner-left {
	border-radius: 3px 0 0 3px;
	border-left: 1px solid #ccc;
}
.customize-control-radio-buttonset label.ui-corner-right {
	border-radius: 0 3px 3px 0;
}
.customize-control-radio-image .image.ui-buttonset input[type=radio] {
	height: auto; 
}
.customize-control-radio-image .image.ui-buttonset label {
	border: 1px solid transparent;
	display: inline-block;
	margin-right: 5px;
	margin-bottom: 5px; 
}
.customize-control-radio-image .image.ui-buttonset label.ui-state-active {
	background: none;
	border-color: #333; 
}
</style>
<?php
}

add_filter( 'landingpress_style', 'landingpress_style_customize' );
function landingpress_style_customize( $style ) {
	$fields = apply_filters( 'landingpress_customize_controls', array() );
	if ( ! empty( $fields ) ) {
		foreach ( $fields as $field ) {
			$defaults = array(
				'setting'				=> '',
				'type'					=> '',
				'default'				=> '',
				'style'					=> '',
			);
			$field = wp_parse_args( $field, $defaults );
			if ( in_array( $field['type'], array( 'color', 'slider' ) ) && $field['setting'] && !empty( $field['style'] ) ) {
				if ( $value = get_theme_mod( $field['setting'], $field['default'] ) ) {
					$style .= str_replace( '[value]', $value, $field['style'] );
				}
			}
			elseif ( in_array( $field['type'], array( 'radio', 'select', 'radio-buttonset', 'radio-image' ) ) && $field['setting'] && !empty( $field['style'] ) ) {
				if ( $value = get_theme_mod( $field['setting'], $field['default'] ) ) {
					if ( isset( $field['style'][$value] ) ) {
						$style .= $field['style'][$value];
					}
				}
			}
			elseif ( in_array( $field['type'], array( 'checkbox' ) ) && $field['setting'] && !empty( $field['style'] ) ) {
				if ( $value = get_theme_mod( $field['setting'], $field['default'] ) ) {
					if ( $value && $field['style']['on'] ) {
						$style .= $field['style']['on'];
					}
					elseif ( $value && $field['style']['off'] ) {
						$style .= $field['style']['off'];
					}
				}
			}
			elseif ( in_array( $field['type'], array( 'custom-css' ) ) && $field['setting'] ) {
				if ( $value = get_theme_mod( $field['setting'], $field['default'] ) ) {
					if ( function_exists( 'landingpress_minify_css' ) ) {
						$style .= landingpress_minify_css( $value );
					}
					else {
						$style .= $value;
					}
				}
			}
		}
	}
	return $style;
}

add_action( 'customize_preview_init', 'landingpress_customize_preview_loading_style_init' );
function landingpress_customize_preview_loading_style_init() {
	global $wp_customize;
	remove_action( 'wp_head', array( $wp_customize, 'customize_preview_loading_style' ) );
	add_action( 'wp_head', 'landingpress_customize_preview_loading_style_head', 100 );
	add_action( 'wp_footer', 'landingpress_customize_preview_loading_style_footer' );
}

function landingpress_customize_preview_loading_style_head() {
	echo '<style>';
	echo '/* Spinkit https://github.com/tobiasahlin/SpinKit MIT License */ ';
	echo 'body.wp-customizer-unloading{opacity:1;cursor:progress!important;-webkit-transition:none;transition:none}body.wp-customizer-unloading *{pointer-events:none!important}body.wp-customizer-unloading .landingpress-customize-loading{display:block!important}.landingpress-customize-loading{display:none;position:fixed;z-index:999999;top:0;left:0;width:100%;height:100%;background:#fff;background:rgba(255,255,255,.8)}.spinkit-wave{display:block;position:relative;top:50%;left:50%;width:50px;height:40px;margin:-25px 0 0 -25px;font-size:10px;text-align:center}.spinkit-wave .spinkit-rect{display:block;float:left;width:6px;height:50px;margin:0 2px;background-color:#e91e63;-webkit-animation:spinkit-wave-stretch-delay 1.2s infinite ease-in-out;animation:spinkit-wave-stretch-delay 1.2s infinite ease-in-out}.spinkit-wave .spinkit-rect1{-webkit-animation-delay:-1.2s;animation-delay:-1.2s}.spinkit-wave .spinkit-rect2{-webkit-animation-delay:-1.1s;animation-delay:-1.1s}.spinkit-wave .spinkit-rect3{-webkit-animation-delay:-1s;animation-delay:-1s}.spinkit-wave .spinkit-rect4{-webkit-animation-delay:-.9s;animation-delay:-.9s}.spinkit-wave .spinkit-rect5{-webkit-animation-delay:-.8s;animation-delay:-.8s}@-webkit-keyframes spinkit-wave-stretch-delay{0%,100%,40%{-webkit-transform:scaleY(.5);transform:scaleY(.5)}20%{-webkit-transform:scaleY(1);transform:scaleY(1)}}@keyframes spinkit-wave-stretch-delay{0%,100%,40%{-webkit-transform:scaleY(.5);transform:scaleY(.5)}20%{-webkit-transform:scaleY(1);transform:scaleY(1)}}';
	echo '</style>';
}

function landingpress_customize_preview_loading_style_footer() {
	echo '<div class="landingpress-customize-loading">';
		echo '<div class="spinkit-wave">';
			echo '<div class="spinkit-rect spinkit-rect1"></div>';
			echo '<div class="spinkit-rect spinkit-rect2"></div>';
			echo '<div class="spinkit-rect spinkit-rect3"></div>';
			echo '<div class="spinkit-rect spinkit-rect4"></div>';
			echo '<div class="spinkit-rect spinkit-rect5"></div>';
		echo '</div>';
	echo '</div>';
}
