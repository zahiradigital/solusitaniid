<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

$readmore = get_theme_mod( 'landingpress_archive_morelink_text' );
if ( !$readmore ) {
	$readmore = esc_html__( 'Continue reading &rarr;', 'landingpress-wp' );
}
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php 
		$img_opt = get_theme_mod( 'landingpress_archive_image', 'featured' );
		if ( 'none' != $img_opt ) :
			if ( 'thumb-left' == $img_opt ) :
				echo landingpress_get_image( array( 'alt' => get_the_title(), 'link' => 'post', 'size' => 'thumbnail', 'class' => 'alignleft', 'fallback' => 'attachment' ) ); 
			elseif ( 'thumb-right' == $img_opt ) :
				echo landingpress_get_image( array( 'alt' => get_the_title(), 'link' => 'post', 'size' => 'thumbnail', 'class' => 'alignright', 'fallback' => 'attachment' ) ); 
			else :
				echo landingpress_get_image( array( 'alt' => get_the_title(), 'link' => 'post', 'fallback' => 'attachment' ) ); 
			endif;
		endif; 
		the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' );
		if ( get_theme_mod( 'landingpress_archive_meta', '1' ) ) :
			echo '<div class="entry-meta">'.landingpress_get_entry_meta().'</div>';
		endif; 
		?>
	</header>
	<?php do_action( 'landingpress_entry_content_before' ); ?>
	<div class="entry-content">
		<?php 
		if ( 'excerpt' == get_theme_mod( 'landingpress_archive_content' ) ) :
			the_excerpt();
			if ( get_theme_mod( 'landingpress_archive_morelink', '1' ) ) :
				echo '<p><a href="'.get_permalink().'" class="more-link">'.esc_html( $readmore ).'</a></p>';
			endif;
		else : 
			the_content( $readmore ); 
			landingpress_link_pages(); 
		endif; 
		?>
	</div>
	<?php do_action( 'landingpress_entry_content_after' ); ?>
</article>
