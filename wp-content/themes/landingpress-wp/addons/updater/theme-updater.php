<?php
/**
 * Easy Digital Downloads Theme Updater
 *
 * @package EDD LandingPress
 * @version 1.0.3
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

// Includes the files needed for the theme updater
if ( !class_exists( 'EDD_Theme_Updater_Admin' ) ) {
	include( dirname( __FILE__ ) . '/theme-updater-admin.php' );
}

// Loads the updater classes
global $landingpress_updater;
$landingpress_updater = new EDD_Theme_Updater_Admin(

	// Config settings
	$config = array(
		'remote_api_url' => LANDINGPRESS_URL, // Site where EDD is hosted
		'item_name'      => LANDINGPRESS_THEME_NAME, // Name of theme
		'theme_slug'     => LANDINGPRESS_THEME_SLUG, // Theme slug
		'version'        => LANDINGPRESS_THEME_VERSION, // The current version of this theme
		'author'         => 'LandingPress', // The author of this theme
		'download_id'    => '', // Optional, used for generating a license renewal link
		'renew_url'      => '', // Optional, allows for a custom license renewal link
		'beta'           => false, // Optional, set to true to opt into beta versions
	),

	// Strings
	$strings = array(
		'theme-license'             => __( 'Theme License', 'landingpress-wp' ),
		'enter-key'                 => __( 'Enter your theme license key.', 'landingpress-wp' ),
		'license-key'               => __( 'License Key', 'landingpress-wp' ),
		'license-action'            => __( 'License Action', 'landingpress-wp' ),
		'deactivate-license'        => __( 'Deactivate License For This Website', 'landingpress-wp' ),
		'activate-license'          => __( 'Activate License For This Website', 'landingpress-wp' ),
		'change-license'            => __( 'Change License Key', 'landingpress-wp' ),
		'status-unknown'            => __( 'License status is unknown.', 'landingpress-wp' ),
		'renew'                     => __( 'Renew?', 'landingpress-wp' ),
		'unlimited'                 => __( 'unlimited', 'landingpress-wp' ),
		'license-key-is-active'     => __( 'License key is active.', 'landingpress-wp' ),
		'expires%s'                 => __( 'Expires %s.', 'landingpress-wp' ),
		'expires-never'             => __( 'Lifetime License.', 'landingpress-wp' ),
		'%1$s/%2$-sites'            => __( 'You have %1$s / %2$s sites activated.', 'landingpress-wp' ),
		'license-key-expired-%s'    => __( 'License key expired %s.', 'landingpress-wp' ),
		'license-key-expired'       => __( 'License key has expired.', 'landingpress-wp' ),
		'license-keys-do-not-match' => __( 'License keys do not match.', 'landingpress-wp' ),
		'license-is-inactive'       => __( 'License is inactive.', 'landingpress-wp' ),
		'license-key-is-disabled'   => __( 'License key is disabled.', 'landingpress-wp' ),
		'site-is-inactive'          => __( 'Site is inactive.', 'landingpress-wp' ),
		'license-status-unknown'    => __( 'License status is unknown.', 'landingpress-wp' ),
		'update-notice'             => __( "Updating this theme will lose any customizations you have made. 'Cancel' to stop, 'OK' to update.", 'landingpress-wp' ),
		'update-available'          => __('<strong>%1$s %2$s</strong> is available. <a href="%3$s" class="thickbox" title="%4s">Check out what\'s new</a> or <a href="%5$s"%6$s>update now</a>.', 'landingpress-wp' ),
	)

);
